#!/bin/bash 

script_version="0.2.2 Alpha"

# Check for update 
function getCheckforUpdates() {
	echo "=> Checking for updates..."

    # Pull updates from server 
	if git pull origin master; then {
		sleep 0.5
		
		# Creating directory 
		#mkdir -p $HOME/.config/bus/update/
		
		# Copying info.cfg veriable files to $HOME/.config/bus/update/
		#cp -r update/info.cfg $HOME/.config/bus/update/
	} fi 

    # Old Version 
    source $HOME/.config/bus/update/info.cfg;
    oldVersion=$CURRENT_VERSION

    # Latest Version 
    source update/info.cfg;  
    latestVersion=$CURRENT_VERSION

	# maching version from local updater file 
	if [[ $(echo $oldVersion) == $(echo $latestVersion) ]] ; then {
		echo "=> No new updates"
	} else {
		echo "=> New update available"; sleep 0.5
		
        # show massage 
		cat update/message.txt; sleep 0.5
        
        # Showing version comperison 
        echo; echo "=> Installed Version : $oldVersion"; echo "=> Latest Update     : $latestVersion"; echo 
		
		# Reading user input 
		read -p "=> Proceed for update (y/n) : " proceedForUpdate

		if [[ $proceedForUpdate == 'y' ]]; then {
            bash update/update.bash
		} elif [[ $proceedForUpdate == 'n' ]]; then {
			echo "=> Skipping Update Process"
		} else echo "=> Invalid Option!";
		fi 
	} fi 
}

printf "\n [1] Check for Update\n"
printf " [0] Exit\n\n" 

read -p "console : " getUserInput 

if [[ $getUserInput == 1 ]]; then {
	getCheckforUpdates;
} elif [[ $getUserInput == 0 ]]; then {
	exit 0; 
} fi 
