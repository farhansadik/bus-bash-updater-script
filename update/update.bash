#!/bin/bash 

###################################################
#   PLEASE SPECIFY MANUALLY HERE EVERYTHING       #
#   EVERY TIME YOU'VE TO EDIT THAT SHIT           #
###################################################

infoFile=$HOME/.config/bus/update/info.cfg;
script_version="0.2.2 Alpha"

# Override version
function setOverrideVersions() {
    echo "=> Preparing for updates"; sleep 0.5  

    # Old Version 
    source $HOME/.config/bus/update/info.cfg;
    oldVersion=$CURRENT_VERSION

    # Latest Version 
    source update/info.cfg;  
    latestVersion=$CURRENT_VERSION

    # Overriding old version variable ith latest version variable 
    echo "=> Overriding old version variable ith latest version variable"
    sed -i -r "s/$oldVersion/$latestVersion/g" $infoFile

    echo "=> Updated to $CURRENT_VERSION" 
}

# Must run this function on every updates 
setOverrideVersions
