# BUS - Bash Updater Script 

![](workflow/concept.png)

## Basic Summery 
I've created thid project as experimental project. The main perpose of this project is to receive updates from git server for bash project. I was just trying to create a basic and simple way to push/pull method. That's it. 

## Directory Structure 
```
app/
├── autoStartScript.bash
├── run.bash
└── update
    ├── info.cfg
    ├── message.txt
    └── update.bash
```


### Requirements 
At first you must copy `info.cfg` into `$HOME/.config/bus/update/` directory. Every time when script will check for updates this file will be checked. Also you can paste your updates files/process scripts into `$HOME/.config/bus/update/` directory. 


#### Implementation 

![](workflow/update_process.png)

1. You need to implement this function which will check for updates...and download the latest updates from git server. 
```bash
function getCheckforUpdates() {

    # Pull updates from server 
    git pull origin master

    # Old Version 
    source $HOME/.config/bus/update/info.cfg;
    oldVersion=$CURRENT_VERSION

    # Latest Version 
    source update/info.cfg;  
    latestVersion=$CURRENT_VERSION
    
    # maching version from local updater file 
    if [[ $(echo $oldVersion) == $(echo $latestVersion) ]] ; then {
        echo "=> No new updates"
    } else {
        echo "=> New update available"; sleep 0.5
		
        # show massage 
        cat update/message.txt; sleep 0.5
        
        # Showing version comperison 
        echo; 
        echo "=> Installed Version : $oldVersion"; 
        echo "=> Latest Update     : $latestVersion"; 
        echo 

        # Reading user input 
        read -p "=> Proceed for update (y/n) : " proceedForUpdate

        if [[ $proceedForUpdate == 'y' ]]; then {
            bash update/update.bash
        } elif [[ $proceedForUpdate == 'n' ]]; then {
            echo "=> Skipping Update Process"
        } else echo "=> Invalid Option!"; fi 
    } fi 
}
```

2. Then specify everything on `update/update.bash`. What changes you've made for this update. After that you must call `setOverrideVersions` function on `update.bash` file for override version number. 

3. After that you'll have to update version number on `update/info.cfg` with specific variable which is:
```bash 
CURRENT_VERSION='0.1.06 Alpha'
```
Also you can include maintainer name or developer name. Do not change the variable name.  

4. For update message you can specify your changes/changelog on `update/message.txt` file. 

**Created By** <br>
Farhan Sadik 